<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/style/style.css">
</head>
<body>
	<header class="header">
    <h1 class="judul" align="center">Toko Jaya Abadi</h1>
    
        <div class="menu">
    <ul>
    <li><a href="<?=base_url();?>Tampilhome/listhome">Home</a></li>
    <li class="dropdown"><a href="#">Master</a>
    	<ul class="isi-dropdown">
    		<li><a href="<?=base_url();?>karyawan/listkaryawan">Data Karyawan</a></li>
    		<li><a href="<?=base_url();?>jabatan/listjabatan">Data Jabatan</a></li>
    		<li><a href="<?=base_url();?>barang/listbarang">Data Barang</a></li>
    		<li><a href="<?=base_url();?>jenis_barang/listjenisbarang">Data Jenis Barang</a></li>
    		<li><a href="<?=base_url();?>supplier/listsupplier">Data Supplier</a></li>
    	</ul>
    </li>
    <li class="dropdown"><a href="#">Transaksi</a>
    	<ul class="isi-dropdown">
    		<li><a href="<?=base_url();?>pembelian/input_h">Pembelian</a></li>
    	</ul>
    </li>
    <li><a href="#">Report</a></li>
    <li><a href="#">Log ut</a></li>
    </ul>
    </div>
    </header>
    <br/>
       
        <div class="blog">
        	<div class="conteudo">
            	<div class="post-info">
        			<b>DATA SUPPLIER</b><br>
                </div>
    
    <ul>
    <h4 align="left">
    <a href="<?=base_url();?>supplier/input">Input Supplier</a></h4>
    </ul>

    <h4 align="right">
  	<label for="Cari Nama"></label>
    <input type="text" name="Cari Nama" id="Cari Nama" placeholder="Cari Nama">
    <input name="cari data" type="button" value="cari data">
  	</h4>
    
    <table width="100%" border="0">
      <tr align="center" bgcolor="#CCCCCC">
        <td>No</td>
        <td>Kode Supplier</td>
        <td>Nama Supplier</td>
        <td>Alamat</td>
        <td>Aksi</td>
      </tr>
<?php
	$no = 0;
	foreach ($data_supplier as $data)
	{
	$no++;
?>
      <tr align="center">
        <td><?=$no;?></td>
        <td><?= $data->kode_supplier; ?></td>
        <td><?= $data->nama_supplier; ?></td>
        <td><?= $data->alamat; ?></td>
        <td><a href="<?=base_url(); ?>supplier/detailsupplier/<?= $data->kode_supplier; ?>">Detail</a>
        | <a href="<?=base_url(); ?>supplier/edit/<?= $data->kode_supplier; ?>">Edit</a>
        | <a href="<?=base_url(); ?>supplier/delete/<?= $data->kode_supplier; ?>
        "onclick="return confirm('Yakin Ingin hapus Data?');">Delete</a>
        </td>
      </tr>
<?php } ?>
    </table>
    </div>
        	</div>

</body>
</html>