<?php
	foreach ($detail_karyawan as $data) {
		$nik			= $data->nik;
		$nama_lengkap	= $data->nama_lengkap;
		$tempat_lahir	= $data->tempat_lahir;
		$tgl_lahir		= $data->tgl_lahir;
		$jenis_kelamin	= $data->jenis_kelamin;
		$alamat			= $data->alamat;
		$telp			= $data->telp;
		$kode_jabatan	= $data->kode_jabatan;
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/style/style.css">
</head>
<body>
	<header class="header">
    <h1 class="judul" align="center">Toko Jaya Abadi</h1>
    
        <div class="menu">
    <ul>
    <li><a href="<?=base_url();?>Tampilhome/listhome">Home</a></li>
    <li class="dropdown"><a href="#">Master</a>
    	<ul class="isi-dropdown">
    		<li><a href="<?=base_url();?>karyawan/listkaryawan">Data Karyawan</a></li>
    		<li><a href="<?=base_url();?>jabatan/listjabatan">Data Jabatan</a></li>
    		<li><a href="<?=base_url();?>barang/listbarang">Data Barang</a></li>
    		<li><a href="<?=base_url();?>jenis_barang/listjenisbarang">Data Jenis Barang</a></li>
    		<li><a href="<?=base_url();?>supplier/listsupplier">Data Supplier</a></li>
    	</ul>
    </li>
    <li><a href="#">Transaksi</a></li>
    <li><a href="#">Report</a></li>
    <li><a href="#">Log ut</a></li>
    </ul>
    </div>
    </header>
    <br/>
    
    	<div class="blog">
        	<div class="conteudo">
            	<div class="post-info">
        			<b>DETAIL DATA KARYAWAN</b><br>
                </div>
            </div>

<table width="1350px" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#3141ff">
<tr>
	<td>NIK</td>
    <td>:</td>
    <td><?= $nik; ?></td>
</tr>
<tr>
	<td>Nama</td>
    <td>:</td>
    <td><?= $nama_lengkap; ?></td>
</tr>
<tr>
	<td>Tempat Lahir</td>
    <td>:</td>
    <td><?= $tempat_lahir; ?></td>
</tr>
<tr>
	<td>Tanggal Lahir</td>
    <td>:</td>
    <td><?= $tgl_lahir; ?></td>
</tr>
<tr>
	<td>Jenis Kelamin</td>
    <td>:</td>
    <td><?= $jenis_kelamin; ?></td>
</tr>
<tr>
	<td>Alamat</td>
    <td>:</td>
    <td><?= $alamat; ?></td>
</tr>
<tr>
	<td>Telp</td>
    <td>:</td>
    <td><?= $telp; ?></td>
</tr>
<tr>
	<td>Kode Jabatan</td>
    <td>:</td>
    <td><?= $kode_jabatan; ?></td>
</tr>
<tr>
    <td></td>
    <td></td>
    <td>
    <a href="<?=base_url();?>Karyawan/listkaryawan">
    <input type="button" name="Submit" id="Submit" value="Kembali Ke Menu Sebelumnya"></a>
    </td>
</tr>
</table>
</div>
</body>
</html>