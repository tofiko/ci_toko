<?php
	foreach ($detail_supplier as $data) {
		$kode_supplier	= $data->kode_supplier;
		$nama_supplier	= $data->nama_supplier;
		$alamat			= $data->alamat;
		$telp			= $data->telp;
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/style/style.css">
</head>
<body>
	<header class="header">
    <h1 class="judul" align="center">Toko Jaya Abadi</h1>
    
        <div class="menu">
    <ul>
    <li><a href="<?=base_url();?>Tampilhome/listhome">Home</a></li>
    <li class="dropdown"><a href="#">Master</a>
    	<ul class="isi-dropdown">
    		<li><a href="<?=base_url();?>karyawan/listkaryawan">Data Karyawan</a></li>
    		<li><a href="<?=base_url();?>jabatan/listjabatan">Data Jabatan</a></li>
    		<li><a href="<?=base_url();?>barang/listbarang">Data Barang</a></li>
    		<li><a href="<?=base_url();?>jenis_barang/listjenisbarang">Data Jenis Barang</a></li>
    		<li><a href="<?=base_url();?>supplier/listsupplier">Data Supplier</a></li>
    	</ul>
    </li>
    <li><a href="#">Transaksi</a></li>
    <li><a href="#">Report</a></li>
    <li><a href="#">Log ut</a></li>
    </ul>
    </div>
    </header>
    <br/>
    
    	<div class="blog">
        	<div class="conteudo">
            	<div class="post-info">
        			<b>DETAIL DATA KARYAWAN</b><br>
                </div>
            </div>

<table width="1350px" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#3141ff">
<tr>
	<td>Kode Supplier</td>
    <td>:</td>
    <td><?= $kode_supplier; ?></td>
</tr>
<tr>
	<td>Nama Supplier</td>
    <td>:</td>
    <td><?= $nama_supplier; ?></td>
</tr>
<tr>
	<td>Alamat</td>
    <td>:</td>
    <td><?= $alamat; ?></td>
</tr>
<tr>
	<td>Telp</td>
    <td>:</td>
    <td><?= $telp; ?></td>
</tr>
<tr>
    <td></td>
    <td></td>
    <td>
    <a href="<?=base_url();?>supplier/listsupplier">
    <input type="button" name="Submit" id="Submit" value="Kembali Ke Menu Sebelumnya"></a>
    </td>
</tr>
</table>
</div>
</body>
</html>