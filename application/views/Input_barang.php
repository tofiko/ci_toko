<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/style/style.css">
</head>
<body>
	<header class="header">
    <h1 class="judul" align="center">Toko Jaya Abadi</h1>
    
        <div class="menu">
    <ul>
    <li><a href="<?=base_url();?>Tampilhome/listhome">Home</a></li>
    <li class="dropdown"><a href="#">Master</a>
    	<ul class="isi-dropdown">
    		<li><a href="<?=base_url();?>karyawan/listkaryawan">Data Karyawan</a></li>
    		<li><a href="<?=base_url();?>jabatan/listjabatan">Data Jabatan</a></li>
    		<li><a href="<?=base_url();?>barang/listbarang">Data Barang</a></li>
    		<li><a href="<?=base_url();?>jenis_barang/listjenisbarang">Data Jenis Barang</a></li>
    		<li><a href="<?=base_url();?>supplier/listsupplier">Data Supplier</a></li>
    	</ul>
    </li>
    <li><a href="#">Transaksi</a></li>
    <li><a href="#">Report</a></li>
    <li><a href="#">Log ut</a></li>
    </ul>
    </div>
    </header>
    <br/>
    
    	<div class="blog">
        	<div class="conteudo">
            	<div class="post-info">
        			<b>INPUT DATA BARANG</b><br>
                </div>
            </div>
   
    <form action="<?=base_url()?>barang/input" method="post">
<table width="1350px" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#3141ff">
  <tr>
    <td>Kode Barang</td>
    <td>:</td>
    <td>
      <input type="text" name="kode_barang" id="kode_barang" maxlength="20">
    </td>
  </tr>
  <tr>
  	<td>Nama Barang</td>
    <td>:</td>
    <td>
      <input type="text" name="nama_barang" id="nama_barang" maxlength="100">
    </td>
  </tr>
  <tr>
    <td>Harga Barang</td>
    <td>:</td>
    <td>
      <input type="text" name="harga_barang" id="harga_barang" maxlength="50">
    </td>
  </tr>
  <tr>
    <td>Nama Jenis</td>
    <td>:</td>
    <td>
      <select name="kode_jenis" id="kode_jenis">
      <?php foreach($data_jenis_barang as $data) {?>
      	<option value="<?= $data->kode_jenis; ?>"><?= $data->nama_jenis; ?></option>
      <?php } ?>
      </select>
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <input type="submit" name="Submit" id="Submit" value="Simpan">
    <input type="reset" name="reset" id="reset" value="Batal">
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <a href="<?=base_url();?>Barang/listbarang">
    <input type="button" name="Submit" id="Submit" value="Kembali Ke Menu Sebelumnya"></a>
    </td>
  </tr>
</table>
</form>
</div>
</body>
</html>