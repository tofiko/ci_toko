<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/style/style.css">
</head>
<body>
	<header class="header">
    <h1 class="judul" align="center">Toko Jaya Abadi</h1>
    
        <div class="menu">
    <ul>
    <li><a href="<?=base_url();?>Tampilhome/listhome">Home</a></li>
    <li class="dropdown"><a href="#">Master</a>
    	<ul class="isi-dropdown">
    		<li><a href="<?=base_url();?>karyawan/listkaryawan">Data Karyawan</a></li>
    		<li><a href="<?=base_url();?>jabatan/listjabatan">Data Jabatan</a></li>
    		<li><a href="<?=base_url();?>barang/listbarang">Data Barang</a></li>
    		<li><a href="<?=base_url();?>jenis_barang/listjenisbarang">Data Jenis Barang</a></li>
    		<li><a href="<?=base_url();?>supplier/listsupplier">Data Supplier</a></li>
    	</ul>
    </li>
    <li><a href="#">Transaksi</a></li>
    <li><a href="#">Report</a></li>
    <li><a href="#">Log ut</a></li>
    </ul>
    </div>
    </header>
    <br/>
    
    	<div class="blog">
        	<div class="conteudo">
            	<div class="post-info">
        			<b>EDIT DATA JABATAN</b><br>
                </div>
            </div>
<?php
	foreach ($detail_jabatan as $data) {
		$kode_jabatan	= $data->kode_jabatan;
		$nama_jabatan	= $data->nama_jabatan;
		$keterangan		= $data->keterangan;
	}
?>
    <form action="<?=base_url()?>jabatan/edit/<?=$kode_jabatan;?>" method="post">

<table width="1350px" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#3141ff">
  <tr>
    <td>Kode Jabatan</td>
    <td>:</td>
    <td>
      <input type="text" name="kode_jabatan" id="kode_jabatan" value="<?=$kode_jabatan;?>" maxlength="50" readonly>
    </td>
  </tr>
  <tr>
    <td>Nama Jabatan</td>
    <td>:</td>
    <td><input type="text" name="nama_jabatan" id="nama_jabatan" value="<?=$nama_jabatan;?>"/></td>
  </tr>
  <tr>
    <td>Keterangan</td>
    <td>:</td>
    <td><input type="text" name="keterangan" id="keterangan" value="<?=$keterangan;?>" /></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <input type="submit" name="Submit" id="Submit" value="Simpan">
    <input type="reset" name="reset" id="reset" value="Batal">
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <a href="<?=base_url();?>Jabatan/listjabatan">
    <input type="button" name="Submit" id="Submit" value="Kembali Ke Menu Sebelumnya"></a>
    </td>
  </tr>
  </form>
</table>
</div>
</body>
</html>