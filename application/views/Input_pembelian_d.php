<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/style/style.css">
</head>
<body>
	<header class="header">
    <h1 class="judul" align="center">Toko Jaya Abadi</h1>
    
        <div class="menu">
    <ul>
    <li><a href="<?=base_url();?>Tampilhome/listhome">Home</a></li>
    <li class="dropdown"><a href="#">Master</a>
    	<ul class="isi-dropdown">
    		<li><a href="<?=base_url();?>karyawan/listkaryawan">Data Karyawan</a></li>
    		<li><a href="<?=base_url();?>jabatan/listjabatan">Data Jabatan</a></li>
    		<li><a href="<?=base_url();?>barang/listbarang">Data Barang</a></li>
    		<li><a href="<?=base_url();?>jenis_barang/listjenisbarang">Data Jenis Barang</a></li>
    		<li><a href="<?=base_url();?>supplier/listsupplier">Data Supplier</a></li>
    	</ul>
    </li>
    <li><a href="#">Transaksi</a></li>
    <li><a href="#">Report</a></li>
    <li><a href="#">Log ut</a></li>
    </ul>
    </div>
    </header>
    <br/>
    
    	<div class="blog">
        	<div class="conteudo">
            	<div class="post-info">
        			<b>INPUT DATA PEMBELIAN DETAIL</b><br>
                </div>
            </div>
   
    <form action="<?=base_url()?>pembelian/input_d" method="post">
<table width="1350px" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#3141ff">
  <tr>
    <td>Nama Barang</td>
    <td>:</td>
    <td>
      <select name="kode_barang" id="kode_barang">
      <?php foreach($data_barang as $data) {?>
      	<option value="<?= $data->kode_barang; ?>"><?= $data->nama_barang; ?></option>
      <?php } ?>
      </select>
    </td>
  </tr>
  <tr>
    <td>Qty</td>
    <td>:</td>
    <td>
      <input type="text" name="qty" id="qty" maxlength="20">
    </td>
  </tr>
  <tr>
    <td>Harga Barang</td>
    <td>:</td>
    <td>
      <input type="text" name="harga_barang" id="harga_barang" maxlength="50">
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <input type="submit" name="Submit" id="Submit" value="Simpan">
    <input type="reset" name="reset" id="reset" value="Batal">
    </td>
  </tr>
  
  <table width="100%" border="0">
      <tr align="center" bgcolor="#CCCCCC">
        <td>No</td>
        <td>Kode Barang</td>
        <td>Nama Barang</td>
        <td>Qty</td>
        <td>Harga</td>
        <td>Jumlah</td>
        </td>
      </tr>
<?php
	$no = 0;
	foreach ($data_pembelian_detail as $data)
	{
	$no++;
?>
      <tr align="center">
        <td><?=$no;?></td>
        <td><?= $data->kode_barang; ?></td>
        <td><?= $data->nama_barang; ?></td>
        <td><?= $data->qty; ?></td>
        <td><?= $data->harga; ?></td>
        <td><?= $data->jumlah; ?></td>
      </tr>
<?php } ?>
    </table>
    
  <tr>
    <td></td>
    <td></td>
    <td>
    <a href="<?=base_url();?>Karyawan/listkaryawan">
    <input type="button" name="Submit" id="Submit" value="Kembali Ke Menu Sebelumnya"></a>
    </td>
  </tr>
</table>
</form>
</div>
</body>
</html>