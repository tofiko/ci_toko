<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pembelian_model extends CI_Model
{
	//panggil nama table
	private $_table_header = "pembelian_header";
	private $_table_detail = "pembelian_detail";
	
	public function tampilDataPembelian()
	{
		$query = $this->db->query(
			"SELECT * FROM " . $this->_table_header . " WHERE flag = 1" );
		return $query->result();
	
	}
	
	public function savePembelianHeader()
	{
		$data['no_transaksi']	= $this->input->post('no_transaksi');
		$data['kode_supplier']	= $this->input->post('kode_supplier');
		$data['tanggal']		= date('Y-m-d');
		$data['approved']		= 1;
		$data['flag']			= 1;
		$this->db->insert($this->_table_header, $data);
	}
	
	public function idTransaksiTerakhir()
	{
		$query = $this->db->query(
			"SELECT * FROM " . $this->_table_header . " WHERE flag = 1 ORDER BY
			id_pembelian_h DESC LIMIT 0,1" );
			$data_id = $query->result();
			
			foreach ($data_id as $data){
				$last_id = $data->id_pembelian_h;
			}
		return $last_id;
	}
	
	public function tampilDataPembelianDetail()
	{
		$query = $this->db->query(
			"SELECT A.id_pembelian_d,B.nama_barang FROM " . $this->_table_detail . " AS A
			INNER JOIN barang AS B ON A.kode_barang=B.kode_barang
			WHERE A.flag = 1 AND A.id_pembelian_h='$id'");
		return $query->result();
	
	}
	
	public function savePembelianDetail()
	{
		$qty	=$this->input->post('qty');
		$harga	=$this->input->post('harga');
		
		$data['id_pembelian_h']	=$this->input->post('id_pembelian_h');
		$data['kode_barang']	=$this->input->post('kode_barang');
		$data['qty']			=$qty;
		$data['harga']			=$harga;
		$data['jumlah']			=$qty * $harga;
		$data['flag']			=1;
		
		$this->db->insert($this->_table_detail,$data);	
	}
}
