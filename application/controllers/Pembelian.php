<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembelian extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		// load model terkait
		$this->load->model("karyawan_model");
		$this->load->model("supplier_model");
		$this->load->model("pembelian_model");
		$this->load->model("barang_model");
	}
	
	public function index()
	{
		$this->listsupplier();
	}
	
	public function listsupplier()
	{
		$data['data_karyawan'] = $this->karyawan_model->tampilDataKaryawan();
		$this->load->view('home_karyawan', $data);
	}
	
	public function input_h()
	{
		$data['data_supplier'] = $this->supplier_model->tampilDataSupplier();
		
		if (!empty($_REQUEST)) {
			$m_pembelian_h = $this->pembelian_model;
			$m_pembelian_h->savePembelianHeader();
			
			//panggil ID transaksi terakhir
			$id_terakhir = $m_pembelian_h->idTransaksiTerakhir();
			
			redirect("pembelian/input_d/" . $id_terakhir, "refresh");
		}
		$this->load->view('input_pembelian', $data);
	}

	public function input_d()
	{
		$data['data_barang'] 			= $this->barang_model->tampilDataBarang();
		$data['id_header']				= $this->pembelian_model->tampilDataPembelian();
		
		if (!empty($_REQUEST)) {
			$m_pembelian_d = $this->pembelian_model;
			$m_pembelian_d->savePembelianDetail();
			
			//panggil ID transaksi terakhir
			$last_id = $m_pembelian_d->idTransaksiTerakhir();
			
			redirect("pembelian/input_d/" . $last_id, "refresh");
		}
		$this->load->view('input_pembelian_d', $data);
	}

		
	/*public function input_d($id_pembelian_header)
	{
		$data['data_barang'] 			= $this->barang_model->tampilDataBarang();
		$data['id_header']				= $id_pembelian_header;
		$data['data_pembelian_detail']	= $this->pembelian_model->tampilDataPembelianDetail($id_pembelian_header);
		
		if (!empty($_REQUEST)) {
			//save detail
			$this->pembelian_model->savePembelianDetail($id_pembelian_header);
			
			//proses update stok
			$kode_barang	= $this->input->post('kode_barang');
			$qty			= $this->input->post('qty');
			$this->barang_model->updateStok($kode_barang, $qty);
			
			redirect("pembelian/input_d/" . $id_pembelian_header, "refresh");
		}
		$this->load->view('input_pembelian_d', $data);
	}*/
}