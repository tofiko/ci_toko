<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		// load model terkait
		$this->load->model("supplier_model");
	}
	
	public function index()
	{
		$this->listsupplier();
	}
	
	public function listsupplier()
	{
		$data['data_supplier'] = $this->supplier_model->tampilDataSupplier();
		$this->load->view('home_supplier', $data);
	}
	
	public function input()
	{
		if (!empty($_REQUEST)) {
			$m_supplier = $this->supplier_model;
			$m_supplier->save();
			redirect("supplier/index", "refresh");
		}
		$this->load->view('input_supplier');
	}
	
	public function detailsupplier($kode_supplier)
	{
		$data['detail_supplier'] = $this->supplier_model->detail($kode_supplier);
		$this->load->view('Detail_supplier' , $data);
	}
	
	public function edit($kode_supplier)
	{
		$data['detail_supplier'] = $this->supplier_model->detail($kode_supplier);
		
		if (!empty($_REQUEST)) {
			$m_supplier = $this->supplier_model;
			$m_supplier->update($kode_supplier);
			redirect("supplier/index", "refresh");
		}
		$this->load->view('edit_supplier', $data);
	}
	
	public function delete($kode_supplier)
	{
		$m_supplier = $this->supplier_model;
		$m_supplier->delete($kode_supplier);
		redirect("supplier/index", "refresh");
	}
	
}